# The Challenge - Part 1


## Automate deployment of the Air Quality API to AWS

I am going to use Terraform w/EC2, autoscaling groups and ELB.
The reason for this is it gives the flexibility of a Linux server as well as
the ease of not having to learn custom services which might be a better fit,
however they will have a steep learning curve for little benefit at the beginning.

This can also be easily transformed into running 1 set of resources for each developer,
branch, etc.

A compromise is the database will not be resilient, and the API will not be easily
scalable with this design.

This is because each EC2 is running API&DB, so creating a 2nd EC2 will mean the
recent queries page will not be deterministic, as it will hit both APIs in a
round-robin fashion.

A proposed solution to this is using AWS's RDS service to host our Postgres database,
or ElastiCache should we choose Redis (part2).

## Make any code changes you feel are necessary to facilitate the deployment

None are very interesting at the moment, however changing the hardcoded database
details to be able to change them through environment variables is one of the first
steps I'd take, along with the API key secret - secrets should never be in SVC.

# The Challenge - Part 2

## A proposed Continuous Delivery pipeline for the Air Quality API

With more time, I would consider either using a container orchestration tool such
as Rancher or Kubernetes to handle deployment, and handle deployments automatically
with [crane, a project I made](https://github.com/kiwicom/crane),
or Spinnaker.io for Kubernetes.

Using AWS Lambda architecture might also be an option with the serverless framework.

## Changes that should be made to the API to facilitate its operation in production

- Redis might be worth using over Postgres
- Monitoring could be done with Prometheus or Datadog
- Lambda might make security easier to configure, with the drawback of limited flexibility

### Database

For the purpose of storing recent queries, Redis might be a better candidate here,
depending on how many recent queries we need to store (business decision).

Right now it will eventually be a bottleneck to `SELECT * FROM queries` without
any limits.

Should we keep historical data, then Postgres could remain and the SELECT could
be limited to the most recent ones. If historical data and resiliency are not
necessary, then Redis could replace Postgres.

### Monitoring

A healthcheck endpoint should be introduced so that an eventual orchestrator can
poll for the health of the container.

Metrics could be handled either in a pull fashion implementing a metrics endpoint
which could be scraped by Prometheus, or pushing to Datadog.

Implementing alerts would be done on the respecrive platforms.

Lambda has the advantage of having some tooling already available, such as metrics
in CloudWatch and logs in CloudWatch logs, worth considering.

### Security

Security-wise, we should probably care more about the underlying infrastructure
than the API itself.

It is possible to abuse the API as it has no ratelimiting nor caching, this might be
worth implementing depending on the impact of the APi going down.

Using EC2, we'll have to care about security ourselves, which means setting up
Security Groups properly, making sure SSH password auth is disabled, and whatnot.

Using a container orchestrator such as Rancher or Kubernetes might make the
infrastructure security more streamlined, as nodes will be managed with `kubectl`
or Rancher's UI rather than via SSH, and they will have similar/same configuration
regardless of what they are running.

With Lambda on the other hand, infrastructure security is offloaded to AWS and
ratelimiting is handled by services such as AWS's API Gateway or CloudFront for
caching, which means we do not have to implement it in the API's business logic.
