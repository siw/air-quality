terraform {
  backend "local" {}
}

# Configure the AWS Provider & AWS related data providers
provider "aws" {
  region = "${var.region}"

  # credentials from ~/.aws/credentials
}

data "aws_region" "current" {
  current = true
}

data "aws_caller_identity" "current" {
  # no arguments
}
