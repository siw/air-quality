## AWS deployment

Make sure your AWS credentials are in $HOME/.aws/credentials

```bash
terraform init
terraform plan
terraform apply
```

It will then be reachable at the ELB `air-quality` created in Ireland. Find the hostname:

```bash
terraform state show aws_elb.air-quality | grep dns_name
```
