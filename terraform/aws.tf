resource "aws_autoscaling_group" "airquality" {
  name_prefix          = "${var.name}-"
  max_size             = 10
  desired_capacity     = 1
  min_size             = 1
  launch_configuration = "${aws_launch_configuration.airquality.name}"
  vpc_zone_identifier  = ["${data.aws_subnet_ids.all.ids}"]
  load_balancers       = ["${aws_elb.air-quality.id}"]

  termination_policies = ["OldestInstance"]

  tag {
    key                 = "Name"
    value               = "${var.name}"
    propagate_at_launch = true
  }
}

resource "aws_security_group" "allow_22_5000" {
  name        = "allow_22_5000"
  description = "Allow ports 22,5000"
  vpc_id      = "${data.aws_vpc.main.id}"

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_80" {
  name        = "allow_80"
  description = "Allow port 80"
  vpc_id      = "${data.aws_vpc.main.id}"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_launch_configuration" "airquality" {
  name_prefix   = "${var.name}-"
  image_id      = "ami-add175d4"              # AWS's own Ubuntu 16.04 image
  instance_type = "t2.micro"
  user_data     = "${file("cloud-init.yml")}"
  ebs_optimized = false

  root_block_device {
    volume_type           = "standard"
    volume_size           = 10
    delete_on_termination = true
  }

  security_groups = [
    "${aws_security_group.allow_22_5000.id}",
  ]

  lifecycle {
    create_before_destroy = true
  }
}

data "aws_vpc" "main" {
  default = true
}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.main.id}"
}

resource "aws_elb" "air-quality" {
  name = "${var.name}"

  subnets = ["${data.aws_subnet_ids.all.ids}"]

  security_groups = [
    "${aws_security_group.allow_80.id}",
  ]

  listener {
    lb_port       = 80
    lb_protocol   = "http"
    instance_port = 5000

    instance_protocol = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 4
    target              = "TCP:5000"
    interval            = 5
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 30
  connection_draining         = true
  connection_draining_timeout = 30
}
