variable "region" {
  description = "The AWS region"
  default     = "eu-west-1"
}

variable "name" {
  description = "Name used to prefix all resources names"
  default     = "air-quality"
}
